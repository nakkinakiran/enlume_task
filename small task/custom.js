// custom scripts
$(document).ready(function(){ 
    $('#first_name_blk').hide();
    $('.step_blk').hide();
    $('#dashboard').show();

    $('#schoolfee').click(function(){
    	$('.step_blk').hide();
    	$('#school-fee-blk').show();
    });

    $('#houserent').click(function(){
    	$('.step_blk').hide();
    	$('#house-rent').show();
    });

    $('#neftdeposit').click(function(){
    	$('.step_blk').hide();
    	$('#neft-deposits').show();
    });

    $('#requestpay').click(function(){
    	$('.step_blk').hide();
    	$('#request-payment').show();
    });

    $('#back').click(function(){
    	$('.step_blk').hide();
    	$('#dashboard').show();
    });

    $('#back2').click(function(){
    	$('.step_blk').hide();
    	$('#dashboard').show();	
    });

    $('#back3').click(function(){
    	$('.step_blk').hide();
    	$('#dashboard').show();
    });

    $('#back4').click(function(){
    	$('.step_blk').hide();
    	$('#dashboard').show();
    });

    $('#first_name').click(function(){
    	$('#first_name_blk').show();
    	$('#payers_list').hide();
    });
    $('#back_btn').click(function(){
    	$('#first_name_blk').hide();
    	$('#payers_list').show();
    });
});

$(document).ready(function () {
	$('.group').hide();
  	$('#selectconvent').change(function () {
    	$('.group').hide();
    	$('#'+$(this).val()).show();
  	});
});

$(document).ready(function () {
	$('.group').hide();
  	$('#selectconvent2').change(function () {
    	$('.group').hide();
    	$('#'+$(this).val()).show();
  	});
});

$(document).ready(function() {
	$('#upi_id').hide();
    $('input:radio[name=bank_upi]').change(function() {
        if (this.value == 'bank') {
            $('#bank_acc').show();
            $('#upi_id').hide();
        }
        else if (this.value == 'upi') {
        	$('#bank_acc').hide();
            $('#upi_id').show();
        }
    });
});